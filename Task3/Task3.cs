﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Task3
{

    class StringsCollection
    {
        public List<string> RussianStrings { get; } = new List<string>();
        public List<string> EnglishStrings { get; } = new List<string>();

        public StringsCollection(IEnumerable<string> russianStrings, IEnumerable<string> englishStrings)
        {
            RussianStrings.AddRange(russianStrings);
            EnglishStrings.AddRange(englishStrings);
        }
    }

    class Task3
    {
        static double GetPetrenkoIndex(string s)
        {
            double n = s.Count(char.IsLetterOrDigit);
            return n * n * n / 2;
        }

        static bool IsNullOrEmpty<T>(IEnumerable<T> sequence) => sequence == null || !sequence.Any();

        static Dictionary<string, List<string>> FindMatches(IEnumerable<string> russianStrings, IEnumerable<string> englishStrings)
        {
            var result = new Dictionary<string, List<string>>();
            var englishResults = new Dictionary<double, List<string>>();
            foreach (string s in englishStrings)
            {
                int separatorIndex = s.LastIndexOf('|');
                if (separatorIndex == -1)
                {
                    throw new ArgumentException($"Строка \"{s}\" имеет неверный формат (не содержит символа \"|\", разделяющего слова и комментарий)");
                }
                string text = s.Substring(0, separatorIndex);
                string comment = s.Substring(separatorIndex + 1);
                double res = GetPetrenkoIndex(text) + GetPetrenkoIndex(comment);
                if (englishResults.ContainsKey(res))
                {
                    englishResults[res].Add(s);
                }
                else
                {
                    englishResults[res] = new List<string> { s };
                }
            }
            foreach (string s in russianStrings)
            {
                double res = GetPetrenkoIndex(s);
                result[s] = new List<string>();
                if (englishResults.ContainsKey(res))
                {
                    result[s].AddRange(englishResults[res]);
                }
            }
            return result;
        }

        static void PrintMatches(IDictionary<string, List<string>> matches)
        {
            foreach (var match in matches)
            {
                Console.WriteLine($"{match.Key}: {(IsNullOrEmpty(match.Value) ? "[нет соответствий]" : string.Join(", ", match.Value))}");
            }
        }


        static void PrintMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Ввести строки с клавиатуры");
            Console.WriteLine("2. Считать строки из файла");
            Console.WriteLine("3. Выход");
        }

        static int GetInput()
        {
            bool canParse = false;
            int i = 0;
            while (!canParse || i < 1 || i > 3)
            {
                PrintMenu();
                canParse = int.TryParse(Console.ReadKey().KeyChar.ToString(), out i);
            }
            return i;
        }

        static void ReadStrings(ICollection<string> dest)
        {
            string curString;
            while (true)
            {
                curString = Console.ReadLine().Trim();
                if (curString == "")
                    continue;
                if (curString == ".")
                    break;
                dest.Add(curString);
            }
        }

        static StringsCollection ReadStringsFromConsole()
        {
            Console.Clear();
            var russianStrings = new HashSet<string>();
            var englishStrings = new HashSet<string>();
            Console.WriteLine("Введите набор строк на русском языке (Enter - окончание ввода текущей строки). Для окончания ввода введите строку \".\"");
            ReadStrings(russianStrings);
            Console.WriteLine("Введите набор строк на английском языке [формат : {строка | комментарий}] (Enter - окончание ввода текущей строки). Для окончания ввода введите строку \".\"");
            ReadStrings(englishStrings);
            return new StringsCollection(russianStrings.Where(s => !string.IsNullOrWhiteSpace(s)), englishStrings.Where(s => !string.IsNullOrWhiteSpace(s)));
        }

        static StringsCollection ReadStringsFromFile()
        {
            Console.Clear();
            Console.WriteLine("Введите путь к текстовому файлу с набором строк (файл должен содержать список русских строк, строкура-разделитель вида \".\" и список английских строк формата {строка | комментарий}");
            string path = Console.ReadLine();
            string[] lines = null;
            try
            {
                lines = File.ReadAllLines(path).Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.Trim()).ToArray();
            }
            catch
            {
                throw new FileNotFoundException($"Файл по адресу {path} не найден или не может быть открыт в данный момент");
            }

            int separatorIndex = Array.IndexOf(lines, ".");
            if (separatorIndex == -1)
            {
                throw new ArgumentException($"Файл по адресу {path} не содержит строки-разделителя русских и английских строк (строки \".\"");
            }
            var russianStrings = new HashSet<string>(lines.Take(separatorIndex));
            var englishStrings = new HashSet<string>(lines.Skip(separatorIndex + 1));
            return new StringsCollection(russianStrings, englishStrings);
        }

        static void Main(string[] args)
        {
            bool exit = false;
            StringsCollection curStrings = null;
            int input;
            bool success = false;
            while (!exit)
            {
                success = false;
                input = GetInput();
                switch (input)
                {
                    case 1:
                        try
                        {
                            curStrings = ReadStringsFromConsole();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine();
                            Console.WriteLine($"Во время считывания строк с клавиатуры возникла ошибка: {ex.Message}");
                        }
                        break;
                    case 2:
                        try
                        {
                            curStrings = ReadStringsFromFile();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine();
                            Console.WriteLine($"Во время считывания строк из файла возникла ошибка: {ex.Message}");
                        }
                        break;
                    case 3:
                        exit = true;
                        break;
                }
                if (!exit)
                {
                    if (success)
                    {
                        try
                        {
                            Console.WriteLine();
                            Console.WriteLine("РЕЗУЛЬТАТ:");
                            PrintMatches(FindMatches(curStrings.RussianStrings, curStrings.EnglishStrings));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine();
                            Console.WriteLine($"Во время обработки строк возникла ошибка: {ex.Message}");
                        }
                    }
                    Console.WriteLine();
                    Console.WriteLine("Для продолжения нажмите любую клавишу");
                    Console.ReadKey();
                }
            }
        }
    }
}
